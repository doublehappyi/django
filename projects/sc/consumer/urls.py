__author__ = 'db'

from django.conf.urls import patterns, url

from consumer import views

urlpatterns = patterns('',
                       url(r'^$', views.index, name='index'),
               url(r'^register$', views.register, name='register'),
               url(r'^registerUser$',views.registerUser,name='registerUser'),
               url(r'^login$',views.login_page,name='login_page'),
               url(r'^loginAuth$',views.login_auth_page,name='login_aut_page'),
               url(r'^auth$',views.auth,name='auth'),
               url(r'logout',views.i_logout,name='i_logout')
)