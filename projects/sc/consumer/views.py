from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
import logging
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
# Create your views here.

def index(request):
    context = {'name':'yisx'}
    return render(request,'consumer/index.html',context)

def register(request):
    context = {'name':'yisx','password':'aaa'}
    return render(request,'consumer/register.html',context)

def registerUser(request):
    username = request.POST['username']
    email = request.POST['email']
    password = request.POST['password']

    user = User.objects.create_user(username,email,password)
    user.save()

    print "username %s" % username
    print "username %s" % password
    return render(request,'consumer/registerSuccess.html',{})


def login_page(request):
    logging.debug("hello world")
    return render(request,'consumer/login.html',{})

def login_auth_page(request):
    username = request.POST['username']
    password = request.POST['password']

    print "usernam eeeee %s" % username

    user = authenticate(username=username,password=password)
    if user is not None:
        if user.is_active:
            login(request,user)
            if request.user.is_authenticated():
                #return render(request,'consumer/loginSuccess.html',{})
                return redirect(request.path)
            else:
                return HttpResponse("aaa")
        else:
            return HttpResponse("user is deactive !")
    else:
        return render(request,'consumer/loginFailure.html',{})

@login_required
def auth(request):
    if request.user.is_authenticated():
        return render(request,'consumer/auth.html',{})
    else:
        return render(request,'consumer/login.html',{})

def i_logout(request):
    logout(request)
    return render(request,'consumer/login.html',{})