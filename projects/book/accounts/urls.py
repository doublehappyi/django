__author__ = 'YI'

from django.conf.urls import patterns, url
from accounts import views

urlpatterns = patterns('',
                        url(r'^$',views.index),
                        url(r'^register$',views.register),
                        url(r'^registerUser$',views.registerUser),
                        url(r'^login/?$', views.login),
                        url(r'^loginUser$',views.loginUser),
)