# Create your views here.

from django.http import HttpResponse,HttpResponseRedirect
from django.shortcuts import render,redirect
from django.contrib import auth
from django.contrib.auth.models import User

def index(request):
    return HttpResponse("welcome to accounts")

def register(request):
    return render(request,'accounts/register.html')

def registerUser(request):
    username = request.POST['username']
    password = request.POST['password']
    email = request.POST['email']
    user = User.objects.create(username=username,password=password,email=email)

    return redirect("/accounts/login")

def login(request):
    return render(request,"accounts/login.html")

def loginUser(request):
    username = request.POST['username']
    password = request.POST['password']
    user = auth.authenticate(username=username,password=password)
    if user is not None:
        auth.login(request,user)
        return HttpResponse("login success !")
    else:
        return HttpResponse("login failed")