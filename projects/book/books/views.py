# Create your views here.

from django.http import HttpResponse, Http404,HttpResponseRedirect
from django.shortcuts import render

from django.contrib import auth


def index(request):
    return HttpResponse("{'name':'yisx','friends':[1,2,3,4]}")


def show_color(request):
    if 'favorite_color' in request.COOKIES:
        return HttpResponse("your favorite color is %s" % request.COOKIES['favorite_color'])
    else:
        return HttpResponse("you don't have a favorite color cookie")


def set_color(request):
    if 'favorite_color' in request.GET:
        response = HttpResponse('favorite_color is now %s' % request.GET['favorite_color'])
        response.set_cookie('favorite_color', request.GET["favorite_color"])

    else:
        response = HttpResponse("you didn't give a favorite_color...")

    return response


def not_found(request):
    if "fav_color" in request.session:
        return HttpResponse('your fav_color is %s' % request.session["fav-color"])
    else:
        request.session['fav_color'] = 'red'
        return HttpResponse("fav_color is not exist !")

def login(request):
    return render(request,'books/login.html')

def redirect(request):
    return HttpResponseRedirect('/books/login/')

def register(request):
    if request.method == 'POST':
        try:
            username = request.POST['username']
            password = request.POST['password']
            auth.loin(request)

        except KeyError:
            return HttpResponse("form error!")

    else:
        return HttpResponse("POST method is needed !")