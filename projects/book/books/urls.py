__author__ = 'db'

from django.conf.urls import patterns, url

from books import views

urlpatterns = patterns('',
                       url(r'^$', views.index),
                       url(r'^show_color/?$',views.show_color),
                       url(r'^set_color/?$',views.set_color),
                       url(r'^404/?$',views.not_found),
                       url(r'^login/?$',views.login),
                       url(r'^redirect$',views.redirect)
)