from django.conf.urls import patterns, url
from hello import views

urlpatterns = patterns('',
                       url(r'^$', views.index, name='index'),
                       url(r'^search_form$', views.search_form),
                       url(r'^plus/(\d?)$', views.plus)
)
