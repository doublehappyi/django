# Create your views here.

from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    try:
        ua = request.META["HTTP_USER_AGENT"]
    except KeyError:
        ua = 'unknown'
    return HttpResponse("your browser is %s" % ua)

def search_form(request):
    return render(request,'hello/search_form.html')

def plus(request,d):
    try:
        d = d
    except KeyError:
        d = "no d is transffered"
    return HttpResponse(d)
